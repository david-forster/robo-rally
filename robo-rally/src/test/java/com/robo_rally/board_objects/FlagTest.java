package com.robo_rally.board_objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.robo_rally.utils.TestHooks;

@DisplayName("Flag Tests")
class FlagTest {

	Player testPlayer;
	
	@BeforeAll
	public static void init() {
		TestHooks.initiateThread();
	}

	@BeforeEach
	public void resetPlayer() { 
		testPlayer = new Player("Test Player A", new char[][]{{'F','W','F','W','R'}}, 'A');
	}
	
	@Test
    @DisplayName("Player collects a flag")
	public void playerCollectsFlag() {
		assertEquals(0, testPlayer.getLastCapturedFlag());
		
		Flag flg = new Flag(1);
		flg.action(testPlayer);
		
		assertEquals(1, testPlayer.getLastCapturedFlag());
	}
	
	@Test
	public void createMinimumNewFlag() {
		Flag flag = new Flag(1);
		assertEquals(1, flag.getFlagValue());
	}

	@Test
	public void createMaximumNewFlag() { 
		Flag flag = new Flag(4);
		assertEquals(4, flag.getFlagValue());
	}
	
	@Test
	public void createFlagWithValueTooLow() { 
		try { 
			new Flag(0);
		}
		catch(IllegalArgumentException iae) { 
			assertEquals("The flag must have a value between 1 and 4. Passed flag has a value of: 0", iae.getMessage());
		}
	}
	
	@Test
	public void createFlagWithValueTooHigh() { 
		try { 
			new Flag(5);
		}
		catch(IllegalArgumentException iae) { 
			assertEquals("The flag must have a value between 1 and 4. Passed flag has a value of: 5", iae.getMessage());
		}
	}
	
	@Test
    @DisplayName("Player doesn't collect a flag - flag value not next one")
	public void playerDoesntCollectFlagValueTooHigh() {
		assertEquals(0, testPlayer.getLastCapturedFlag());
		
		Flag flg = new Flag(2);
		flg.action(testPlayer);
		
		assertEquals(0, testPlayer.getLastCapturedFlag());
	}
	
	@Test
    @DisplayName("Player doesn't collect a flag - flag value same as player last collected")
	public void playerDoesntCollectFlagValueSame() {
		testPlayer.setLastCapturedFlag(1);
		assertEquals(1, testPlayer.getLastCapturedFlag());
		
		Flag flg = new Flag(1);
		flg.action(testPlayer);
		
		assertEquals(1, testPlayer.getLastCapturedFlag());
	}
	
	@Test
    @DisplayName("Player doesn't collect a flag - flag value lower than player last collected")
	public void playerDoesntCollectFlagValueTooLow() {
		testPlayer.setLastCapturedFlag(2);
		assertEquals(2, testPlayer.getLastCapturedFlag());
		
		Flag flg = new Flag(1);
		flg.action(testPlayer);
		
		assertEquals(2, testPlayer.getLastCapturedFlag());
	}
}