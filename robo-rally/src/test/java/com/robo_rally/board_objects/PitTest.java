package com.robo_rally.board_objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.robo_rally.utils.TestHooks;

@DisplayName("Pit Tests")
public class PitTest {
	
	@BeforeAll
	public static void init() {
		TestHooks.initiateThread();
	}

	@Test
    @DisplayName("Create New Pit")
	public void createNewPit() { 
		Pit pit = new Pit();
		assertNotNull(pit);
	}
	
	@Test
    @DisplayName("Player will respawn")
	public void resetPlayerPosition() { 
		Player player = new Player("TestPlayer", new char[][] {{'A', 'B'}}, 'A');
		Pit pit = new Pit();
		int[] startPosition = {0, 0};
		int[] position = {7, 2};
		
		player.setStartPosition(startPosition);
		player.setPosition(position);
		
		if(player.getPosition() == position && player.getStartPosition() == startPosition) { 
			pit.action(player);
			assertEquals(startPosition, player.getPosition());
		}
		else { 
			assertTrue(false, "Positions were not set correctly on player contruction");
		}
	}
}