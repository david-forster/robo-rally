package com.robo_rally.board_objects;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.robo_rally.utils.TestHooks;

@DisplayName("Player Tests")
public class PlayerTest {
	
	@BeforeAll
	public static void init() {
		TestHooks.initiateThread();
	}
	
	private Player player = new Player("TestName", new char[][] { { 'F', 'L', 'R', 'B', 'U', 'W' } }, 'A');

	@Test
	@DisplayName("Valid Player Test - Can construct Player")
	public void constructPlayer() {
		assertNotNull(player);
	}

	@Test
	@DisplayName("Valid Player Test - Can set Player name")
	public void setPlayerName() {
		assertEquals("TestName", player.getName());
	}

	@Test
	@DisplayName("Valid Player Test - Can set Player Direction")
	public void setValidDirections() {
		String directions = "";

		player.setDirection('N');
		directions += player.getDirection();
		player.setDirection('E');
		directions += player.getDirection();
		player.setDirection('S');
		directions += player.getDirection();
		player.setDirection('W');
		directions += player.getDirection();

		assertEquals("NESW", directions);
	}

	@Test
	@DisplayName("Invalid Player Test - Handles invalid direction")
	public void setInvalidDirections() {
		IllegalArgumentException iae = assertThrows(IllegalArgumentException.class, () -> {
			player.setDirection('M');
		});
		assertEquals("M is not a valid direction of: N, E, S, W", iae.getMessage());
	}

	@Test
	@DisplayName("Valid Player Test - Change player's last captured flag")
	public void setNewFlag() {
		String flags = "";

		player.setLastCapturedFlag(1);
		flags += player.getLastCapturedFlag();
		player.setLastCapturedFlag(2);
		flags += player.getLastCapturedFlag();
		player.setLastCapturedFlag(3);
		flags += player.getLastCapturedFlag();
		player.setLastCapturedFlag(4);
		flags += player.getLastCapturedFlag();

		assertEquals("1234", flags);
	}

	@Test
	@DisplayName("Invalid Player Test - Handles invalid flag value")
	public void setInvalidFlags() {
		IllegalArgumentException iae = assertThrows(IllegalArgumentException.class, () -> {
			player.setLastCapturedFlag(0);
		});
		assertEquals("Invalid flag value: 0. Flag must be between 1 and 4", iae.getMessage());

		iae = assertThrows(IllegalArgumentException.class, () -> {
			player.setLastCapturedFlag(5);
		});
		assertEquals("Invalid flag value: 5. Flag must be between 1 and 4", iae.getMessage());
	}

	@Test
	@DisplayName("Valid Player Test - Can set player position")
	public void setPositions() {
		player.setStartPosition(new int[] { 0, 1 });
		player.setPosition(player.getStartPosition());

		assertArrayEquals(new int[] { 0, 1 }, player.getStartPosition());
		assertArrayEquals(new int[] { 0, 1 }, player.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Player can respawn at start position")
	public void respawnAtStartPosition() {
		player.setStartPosition(new int[] { 6, 2 });
		player.setPosition(new int[] { 3, 6 });
		player.respawn();
		assertArrayEquals(new int[] { 6, 2 }, player.getStartPosition());
		assertArrayEquals(new int[] { 6, 2 }, player.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Moves Player Forward North")
	public void playerFacingNorthWithForwardAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'F'}}, 'A');
		int[] expectedPosition = {0, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('N');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Moves Player Forward East")
	public void playerFacingEastWithForwardAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'F'}}, 'A');
		int[] expectedPosition = {1, 2};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('E');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Moves Player Forward South")
	public void playerFacingSouthWithForwardAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'F'}}, 'A');
		int[] expectedPosition = {2, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('S');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Moves Player Forward West")
	public void playerFacingWestWithForwardAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'F'}}, 'A');
		int[] expectedPosition = {1, 0};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('W');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Moves Player Backward North")
	public void playerFacingNorthWithBackwardAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'B'}}, 'A');
		int[] expectedPosition = {2, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('N');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Moves Player Backward East")
	public void playerFacingEastWithBackwardAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'B'}}, 'A');
		int[] expectedPosition = {1, 0};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('E');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Moves Player Backward South")
	public void playerFacingSouthWithBackwardAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'B'}}, 'A');
		int[] expectedPosition = {0, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('S');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Moves Player Backward West")
	public void playerFacingWestWithBackwardAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'B'}}, 'A');
		int[] expectedPosition = {1, 2};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('W');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
	}
	
	@Test
	@DisplayName("Valid Player Test - Rotate Left from facing North")
	public void playerFacingNorthWithLeftRotateAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'L'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('N');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('W', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Rotate Left from facing East")
	public void playerFacingEastWithLeftRotateAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'L'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('E');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('N', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Rotate Left from facing South")
	public void playerFacingSouthWithLeftRotateAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'L'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('S');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('E', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Rotate Left from facing West")
	public void playerFacingWestWithLeftRotateAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'L'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('W');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('S', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Rotate Right from facing North")
	public void playerFacingNorthWithRightRotateAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'R'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('N');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('E', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Rotate Right from facing East")
	public void playerFacingEastWithRightRotateAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'R'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('E');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('S', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Rotate Right from facing South")
	public void playerFacingSouthWithRightRotateAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'R'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('S');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('W', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Rotate Right from facing West")
	public void playerFacingWestWithRightRotateAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'R'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('W');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('N', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Perform U turn from facing North")
	public void playerFacingNorthWithUTurnAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'U'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('N');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('S', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Perform U turn from facing East")
	public void playerFacingEastWithUTurnAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'U'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('E');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('W', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Perform U turn from facing South")
	public void playerFacingSouthWithUTurnAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'U'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('S');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('N', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Perform U turn from facing West")
	public void playerFacingWestWithUTurnAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'U'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('W');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('E', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Player Waits while facing North")
	public void playerFacingNorthWithSkipTurnAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'W'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('N');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('N', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Player Waits while facing East")
	public void playerFacingEastWithSkipTurnAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'W'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('E');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('E', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Player Waits while facing South")
	public void playerFacingSouthWithSkipTurnAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'W'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('S');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('S', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Player Waits while facing West")
	public void playerFacingWestWithSkipTurnAction() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'W'}}, 'A');
		int[] expectedPosition = {1, 1};
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('W');
		actionPlayer.action();
		assertArrayEquals(expectedPosition, actionPlayer.getPosition());
		assertEquals('W', actionPlayer.getDirection());
	}
	
	@Test
	@DisplayName("Valid Player Test - Player completes multiple moves")
	public void playerCanRunWithMultipleMoves() { 
		Player actionPlayer = new Player("TestName",
				new char[][] {{'F'},{'B'},{'L'},{'R'},{'U'},{'W'}}, 'A');
		actionPlayer.setPosition(new int[] {1, 1});
		actionPlayer.setDirection('N');
		
		//Run forwards move
		actionPlayer.action();
		assertArrayEquals(new int[] {0, 1}, actionPlayer.getPosition());
		
		//Run backwards move
		actionPlayer.action();
		assertArrayEquals(new int[] {1, 1}, actionPlayer.getPosition());
		
		//Run left rotate
		actionPlayer.action();
		assertArrayEquals(new int[] {1, 1}, actionPlayer.getPosition());
		assertEquals('W', actionPlayer.getDirection());
		
		//Run right rotate
		actionPlayer.action();
		assertArrayEquals(new int[] {1, 1}, actionPlayer.getPosition());
		assertEquals('N', actionPlayer.getDirection());
		
		//Run U-Turn rotate
		actionPlayer.action();
		assertArrayEquals(new int[] {1, 1}, actionPlayer.getPosition());
		assertEquals('S', actionPlayer.getDirection());
		
		//Run Wait move
		actionPlayer.action();
		assertArrayEquals(new int[] {1, 1}, actionPlayer.getPosition());
		assertEquals('S', actionPlayer.getDirection());
	}
}