package com.robo_rally.board_objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.robo_rally.utils.TestHooks;

@DisplayName("Gear Tests")
public class GearTest {

	Player testPlayer;
	
	@BeforeAll
	public static void init() {
		TestHooks.initiateThread();
	}
	
	@BeforeEach
	public void resetPlayer() { 
		testPlayer = new Player("Test Player A", new char[][]{{'F','W','F','W','R'}}, 'A');
	}

	@Test
    @DisplayName("Valid Gear Test - Create New Clockwise Gear")
	public void createNewClockwiseGear() {
		Gear gear = new Gear('+');
		assertNotNull(gear);
	}

	@Test
    @DisplayName("Valid Gear Test - Create New Anti-Clockwise Gear")
	public void createNewAntiClockwiseGear() {
		Gear gear = new Gear('-');
		assertNotNull(gear);
	}

	@Test
    @DisplayName("Invalid Gear Test - No gear created")
	public void createNewInvalidGear() {
		try {
			new Gear('A');
		} catch (IllegalArgumentException iae) {
			assertEquals("Could not create a gear using the character: A. Direction must be one of: + or -",
					iae.getMessage());
		}
	}

	@Test
    @DisplayName("Valid Gear Test - Rotate Player Clockwise to face East")
	public void setPlayerClockwiseNorth() {
		Player player = new Player("TestPlayer", new char[][] {{ 'A', 'B' }}, 'A');
		Gear gear = new Gear('+');

		player.setDirection('N');
		if (player.getDirection() == 'N') {
			gear.action(player);
			assertEquals('E', player.getDirection());
		} else {
			assertTrue(false, "Expected Player to have an initial value of N, but was " + player.getDirection());
		}
	}

	@Test
    @DisplayName("Valid Gear Test - Rotate Player Clockwise to face South")
	public void setPlayerClockwiseEast() {
		Player player = new Player("TestPlayer", new char[][] {{ 'A', 'B' }}, 'A');
		Gear gear = new Gear('+');

		player.setDirection('E');
		if (player.getDirection() == 'E') {
			gear.action(player);
			assertEquals('S', player.getDirection());
		} else {
			assertTrue(false, "Expected Player to have an initial value of E, but was " + player.getDirection());
		}
	}

	@Test
    @DisplayName("Valid Gear Test - Rotate Player Clockwise to face West")
	public void setPlayerClockwiseSouth() {
		Player player = new Player("TestPlayer", new char[][] {{ 'A', 'B' }}, 'A');
		Gear gear = new Gear('+');

		player.setDirection('S');
		if (player.getDirection() == 'S') {
			gear.action(player);
			assertEquals('W', player.getDirection());
		} else {
			assertTrue(false, "Expected Player to have an initial value of S, but was " + player.getDirection());
		}
	}

	@Test
    @DisplayName("Valid Gear Test - Rotate Player Clockwise to face North")
	public void setPlayerClockwiseWest() {
		Player player = new Player("TestPlayer", new char[][] {{ 'A', 'B' }}, 'A');
		Gear gear = new Gear('+');

		player.setDirection('W');
		if (player.getDirection() == 'W') {
			gear.action(player);
			assertEquals('N', player.getDirection());
		} else {
			assertTrue(false, "Expected Player to have an initial value of W, but was " + player.getDirection());
		}
	}
	
	@Test
    @DisplayName("Valid Gear Test - Rotate Player Anti-Clockwise to face West")
	public void setPlayerAntiClockwiseNorth() {
		Player player = new Player("TestPlayer", new char[][] {{ 'A', 'B' }}, 'A');
		Gear gear = new Gear('-');

		player.setDirection('N');
		if (player.getDirection() == 'N') {
			gear.action(player);
			assertEquals('W', player.getDirection());
		} else {
			assertTrue(false, "Expected Player to have an initial value of N, but was " + player.getDirection());
		}
	}
	
	@Test
    @DisplayName("Valid Gear Test - Rotate Player Anti-Clockwise to face North")
	public void setPlayerAntiClockwiseEast() {
		Player player = new Player("TestPlayer", new char[][] {{ 'A', 'B' }}, 'A');
		Gear gear = new Gear('-');

		player.setDirection('E');
		if (player.getDirection() == 'E') {
			gear.action(player);
			assertEquals('N', player.getDirection());
		} else {
			assertTrue(false, "Expected Player to have an initial value of E, but was " + player.getDirection());
		}
	}
	
	@Test
    @DisplayName("Valid Gear Test - Rotate Player Anti-Clockwise to face East")
	public void setPlayerAntiClockwiseSouth() {
		Player player = new Player("TestPlayer", new char[][] {{ 'A', 'B' }}, 'A');
		Gear gear = new Gear('-');

		player.setDirection('S');
		if (player.getDirection() == 'S') {
			gear.action(player);
			assertEquals('E', player.getDirection());
		} else {
			assertTrue(false, "Expected Player to have an initial value of S, but was " + player.getDirection());
		}
	}
	
	@Test
    @DisplayName("Valid Gear Test - Rotate Player Anti-Clockwise to face South")
	public void setPlayerAntiClockwiseWest() {
		Player player = new Player("TestPlayer", new char[][] {{ 'A', 'B' }}, 'A');
		Gear gear = new Gear('-');

		player.setDirection('W');
		if (player.getDirection() == 'W') {
			gear.action(player);
			assertEquals('S', player.getDirection());
		} else {
			assertTrue(false, "Expected Player to have an initial value of W, but was " + player.getDirection());
		}
	}
}