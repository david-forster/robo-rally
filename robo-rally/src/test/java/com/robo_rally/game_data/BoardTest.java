package com.robo_rally.game_data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.robo_rally.base_objects.BoardAsset;
import com.robo_rally.board_objects.Flag;
import com.robo_rally.board_objects.Gear;
import com.robo_rally.board_objects.Pit;
import com.robo_rally.board_objects.Player;
import com.robo_rally.utils.TestHooks;

@DisplayName("Board Tests")
class BoardTest {
	
	Player testPlayerA;
	Player testPlayerB;
	Player testPlayerC;
	Player testPlayerD;
	
	BoardAsset[][] board1;
	
	BoardAsset[][] board2;
	
	BoardAsset[][] board3;
	
	BoardAsset[][] board4;
	
	BoardAsset[][] board5;
	
	BoardAsset[][] board6;
	
	BoardAsset[][] board7;
	
	@BeforeAll
	public static void init() {
		TestHooks.initiateThread();
	}
	
	@BeforeEach
	public void recreateData() {
		testPlayerA = new Player("Test Player A", new char[][]{{'F','W','F','W','R'}}, 'A');
		testPlayerB = new Player("Test Player B", new char[][]{{'F','W','F','W','R'}}, 'B');
		testPlayerC = new Player("Test Player C", new char[][]{{'F','W','F','W','R'}}, 'C');
		testPlayerD = new Player("Test Player D", new char[][]{{'F','W','F','W','R'}}, 'D');
		
		board1 = new BoardAsset[][] {
			{null, null, null, testPlayerA},
			{null, testPlayerD, null, null},
			{null, null, null, testPlayerC},
			{null, null, null, null},
			{null, null, testPlayerB, null}
		};
			
		board2 = new BoardAsset[][] {
			{null, testPlayerC, null, null},
			{null, null, null, null},
			{testPlayerA, null, null, null},
			{null, null, testPlayerD, null},
			{null, testPlayerB, null, null}
		};
		
		board3 = new BoardAsset[][] {
			{null, null, null, new Pit()},
			{testPlayerD, new Gear('+'), null, testPlayerA},
			{null, null, null, new Flag(1)},
			{null, null, null, testPlayerC},
			{null, testPlayerB, new Gear('-'), null}
		};
		
		board4 = new BoardAsset[][] {
			{null, null, null, testPlayerD},
			{null, testPlayerA, null, null},
			{null, null, null, testPlayerC},
			{null, null, null, null},
			{null, null, testPlayerB, null}
		};
		
		board5 = new BoardAsset[][] {
			{null, testPlayerC, null, testPlayerD},
			{null, testPlayerA, null, null},
			{null, null, null, null},
			{null, null, null, null},
			{null, null, testPlayerB, null}
		};
		
		board6 = new BoardAsset[][] {
			{null, testPlayerC, null, testPlayerD},
			{null, testPlayerA, testPlayerB, null},
			{null, null, null, null},
			{null, null, null, null},
			{null, null, null, null}
		};
		
		board7 = new BoardAsset[][] {
			{null, testPlayerC, null, testPlayerD},
			{null, testPlayerA, testPlayerB, null},
			{null, new Gear('+'), null, null},
			{null, null, null, null},
			{null, null, null, null}
		};
	}
	
	@Test
    @DisplayName("No Assets triggered - Same state")
	public void nothingTriggeredSameState() {
		Board brd = new Board(board1, board1, 0);
		
		assertEquals(board1, brd.getInitialState());
		assertEquals(board1, brd.getCurrentState());
		assertEquals(testPlayerA, (Player)brd.getCurrentState()[0][3]);
		assertEquals(testPlayerB, (Player)brd.getCurrentState()[4][2]);
		assertEquals(testPlayerC, (Player)brd.getCurrentState()[2][3]);
		assertEquals(testPlayerD, (Player)brd.getCurrentState()[1][1]);
		
		assertEquals(null, brd.activate());
		
		assertEquals(board1, brd.getInitialState());
		assertEquals(board1, brd.getCurrentState());
		assertEquals(testPlayerA, (Player)brd.getCurrentState()[0][3]);
		assertEquals(testPlayerB, (Player)brd.getCurrentState()[4][2]);
		assertEquals(testPlayerC, (Player)brd.getCurrentState()[2][3]);
		assertEquals(testPlayerD, (Player)brd.getCurrentState()[1][1]);
	}
	
	@Test
    @DisplayName("No Assets triggered - Different states")
	public void nothingTriggeredDifferentState() {
		Board brd = new Board(board1, board2, 0);
		
		assertEquals(board1, brd.getInitialState());
		assertEquals(board2, brd.getCurrentState());
		assertEquals(testPlayerA, (Player)brd.getCurrentState()[2][0]);
		assertEquals(testPlayerB, (Player)brd.getCurrentState()[4][1]);
		assertEquals(testPlayerC, (Player)brd.getCurrentState()[0][1]);
		assertEquals(testPlayerD, (Player)brd.getCurrentState()[3][2]);
		
		assertEquals(null, brd.activate());
		
		assertEquals(board1, brd.getInitialState());
		assertEquals(board2, brd.getCurrentState());
		assertEquals(testPlayerA, (Player)brd.getCurrentState()[2][0]);
		assertEquals(testPlayerB, (Player)brd.getCurrentState()[4][1]);
		assertEquals(testPlayerC, (Player)brd.getCurrentState()[0][1]);
		assertEquals(testPlayerD, (Player)brd.getCurrentState()[3][2]);
	}
	
	@Test
    @DisplayName("Board events triggered - all players changed")
	public void boardTriggeredAllPlayers() {
		Board brd = new Board(board3, board1, 4);
		testPlayerA.setPosition(new int[] {0, 3});
		testPlayerA.setStartPosition(new int[] {1, 3});
		
		assertEquals(testPlayerA, brd.getCurrentState()[0][3]);
		assertEquals(testPlayerA.getPosition(), ((Player)brd.getCurrentState()[0][3]).getPosition());
		assertEquals('N', ((Player)brd.getCurrentState()[4][2]).getDirection());
		assertEquals(0, ((Player)brd.getCurrentState()[2][3]).getLastCapturedFlag());
		assertEquals('N', ((Player)brd.getCurrentState()[4][2]).getDirection());
		
		assertEquals(brd.getInitialState(), board3);
		assertEquals(brd.getCurrentState(), board1);
		
		assertEquals(null, brd.activate());
		
		assertEquals(testPlayerA, (Player)brd.getCurrentState()[1][3]);
		assertEquals(testPlayerA.getStartPosition(), ((Player)brd.getCurrentState()[1][3]).getPosition());
		assertEquals('W', ((Player)brd.getCurrentState()[4][2]).getDirection());
		assertEquals(1, ((Player)brd.getCurrentState()[2][3]).getLastCapturedFlag());
		assertEquals('E', ((Player)brd.getCurrentState()[1][1]).getDirection());
	}
	
	@Test
    @DisplayName("Board events triggered - Player Wins")
	public void boardTriggeredPlayerWins() {
		Board brd = new Board(board3, board1, 1);
		testPlayerA.setPosition(new int[] {0, 3});
		testPlayerA.setStartPosition(new int[] {1, 3});
		
		assertEquals(testPlayerA, brd.getCurrentState()[0][3]);
		assertEquals(testPlayerA.getPosition(), ((Player)brd.getCurrentState()[0][3]).getPosition());
		assertEquals('N', ((Player)brd.getCurrentState()[4][2]).getDirection());
		assertEquals(0, ((Player)brd.getCurrentState()[2][3]).getLastCapturedFlag());
		assertEquals('N', ((Player)brd.getCurrentState()[4][2]).getDirection());
		
		assertEquals(brd.getInitialState(), board3);
		assertEquals(brd.getCurrentState(), board1);
		
		assertEquals("Test Player C", brd.activate());
		
		assertEquals(testPlayerA, (Player)brd.getCurrentState()[1][3]);
		assertEquals(testPlayerA.getStartPosition(), ((Player)brd.getCurrentState()[1][3]).getPosition());
		assertEquals(1, ((Player)brd.getCurrentState()[2][3]).getLastCapturedFlag());
		assertEquals('E', ((Player)brd.getCurrentState()[1][1]).getDirection());

		assertEquals('N', ((Player)brd.getCurrentState()[4][2]).getDirection()); //Won't be changed as Player C won
	}

	@Test
    @DisplayName("Board find free respawn point - default position")
	public void boardFindRespawnDefault() {
		Board brd = new Board(board3, board1, 0);
		testPlayerD.setPosition(new int[] {1, 1});
		testPlayerD.setStartPosition(new int[] {1, 0});
		
		assertEquals(testPlayerD, brd.getCurrentState()[1][1]);
		assertEquals(testPlayerD.getPosition(), ((Player)brd.getCurrentState()[1][1]).getPosition());

		assertEquals(brd.getInitialState(), board3);
		assertEquals(brd.getCurrentState(), board1);
		
		testPlayerD.respawn();
		int[] resp = brd.findFreeRespawnPoint(testPlayerD);
		
		assertEquals(1, resp[0]);
		assertEquals(0, resp[1]);
	}
	
	@Test
    @DisplayName("Board find free respawn point - north position")
	public void boardFindRespawnNorth() {
		Board brd = new Board(board1, board4, 0);
		testPlayerD.setPosition(new int[] {0, 3});
		testPlayerD.setStartPosition(new int[] {1, 1});
		
		assertEquals(testPlayerD, brd.getCurrentState()[0][3]);
		assertEquals(testPlayerD.getPosition(), ((Player)brd.getCurrentState()[0][3]).getPosition());

		assertEquals(brd.getInitialState(), board1);
		assertEquals(brd.getCurrentState(), board4);
		
		testPlayerD.respawn();
		int[] resp = brd.findFreeRespawnPoint(testPlayerD);
		
		assertEquals(0, resp[0]);
		assertEquals(1, resp[1]);
	}

	@Test
    @DisplayName("Board find free respawn point - east position")
	public void boardFindRespawnEast() {
		Board brd = new Board(board1, board5, 0);
		testPlayerD.setPosition(new int[] {0, 3});
		testPlayerD.setStartPosition(new int[] {1, 1});
		
		assertEquals(testPlayerD, brd.getCurrentState()[0][3]);
		assertEquals(testPlayerD.getPosition(), ((Player)brd.getCurrentState()[0][3]).getPosition());

		assertEquals(brd.getInitialState(), board1);
		assertEquals(brd.getCurrentState(), board5);
		
		testPlayerD.respawn();
		int[] resp = brd.findFreeRespawnPoint(testPlayerD);
		
		assertEquals(1, resp[0]);
		assertEquals(2, resp[1]);
	}
	
	@Test
    @DisplayName("Board find free respawn point - south position")
	public void boardFindRespawnSouth() {
		Board brd = new Board(board1, board6, 0);
		testPlayerD.setPosition(new int[] {0, 3});
		testPlayerD.setStartPosition(new int[] {1, 1});
		
		assertEquals(testPlayerD, brd.getCurrentState()[0][3]);
		assertEquals(testPlayerD.getPosition(), ((Player)brd.getCurrentState()[0][3]).getPosition());

		assertEquals(brd.getInitialState(), board1);
		assertEquals(brd.getCurrentState(), board6);
		
		testPlayerD.respawn();
		int[] resp = brd.findFreeRespawnPoint(testPlayerD);
		
		assertEquals(2, resp[0]);
		assertEquals(1, resp[1]);
	}
	
	@Test
    @DisplayName("Board find free respawn point - west position")
	public void boardFindRespawnWest() {
		Board brd = new Board(board1, board7, 0);
		testPlayerD.setPosition(new int[] {0, 3});
		testPlayerD.setStartPosition(new int[] {1, 1});
		
		assertEquals(testPlayerD, brd.getCurrentState()[0][3]);
		assertEquals(testPlayerD.getPosition(), ((Player)brd.getCurrentState()[0][3]).getPosition());

		assertEquals(brd.getInitialState(), board1);
		assertEquals(brd.getCurrentState(), board7);
		
		testPlayerD.respawn();
		int[] resp = brd.findFreeRespawnPoint(testPlayerD);
		
		assertEquals(1, resp[0]);
		assertEquals(0, resp[1]);
	}
}