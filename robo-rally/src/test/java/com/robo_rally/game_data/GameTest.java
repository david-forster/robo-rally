package com.robo_rally.game_data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.robo_rally.board_objects.Flag;
import com.robo_rally.board_objects.Gear;
import com.robo_rally.board_objects.Pit;
import com.robo_rally.board_objects.Player;
import com.robo_rally.utils.TestHooks;


public class GameTest {
	
	@BeforeAll
	public static void init() {
		TestHooks.initiateThread();
	}

	private String[] testNames = {"TestName1", "TestName2", "TestName3"};
	//This is just for formatting so that I can read it 
	private char[][][] testMoves = {
			{{'F', 'L', 'R', 'B'}, {'B', 'F', 'W', 'F'}, {'U', 'F', 'L', 'R'}},
			{{'U', 'F', 'L', 'R'}, {'B', 'F', 'W', 'F'}, {'F', 'L', 'R', 'B'}},
			{{'B', 'F', 'W', 'F'}, {'F', 'L', 'R', 'B'}, {'U', 'F', 'L', 'R'}}
	};
	private char[][] testBoard = {
			{'.','.','.','.','.','.'},
			{'.','.','.','.','.','.'},
			{'.','.','.','.','.','.'},
			{'.','.','.','.','.','.'},
			{'.','.','.','.','.','.'}
		};
	Game game = new Game(testNames, testMoves, testBoard, null);
	
	@Test
	public void createAGameUsingPassedParameters() { 
		assertNotNull(game);
	}
	
	@Test
	public void createAccuratePlayerOrderFromGameConstructor() { 
		String mapValues = game.getPlayerOrder().entrySet().stream()
		.map(e -> e.getKey().getName() + " " + e.getValue().getName())
		.collect(Collectors.joining());
		//Check that they loop back on each other
		if(mapValues.contains("TestName1 TestName2") && mapValues.contains("TestName2 TestName3") && mapValues.contains("TestName3 TestName1")) { 
			assertTrue(true);
		}
		else assertTrue(false, "Player map does not loop");
	}
	
	@Test
	public void createBoardWithPlayerPositions() { 
		char[][] playerBoard = {
				{'.','.','.','.','.','B'},
				{'.','.','.','.','.','.'},
				{'.','.','C','.','.','.'},
				{'.','.','A','.','.','.'},
				{'.','.','.','.','.','.'}
			};
		Game game = new Game(testNames, testMoves, playerBoard, null);
		boolean containsPlayers = Stream.of(game.getBoard().getInitialState())
				.flatMap(Stream::of).anyMatch(i -> i instanceof Player);
		
		Player a = (Player) game.getBoard().getInitialState()[0][5];
		Player b = (Player) game.getBoard().getInitialState()[2][2];
		Player c = (Player) game.getBoard().getInitialState()[3][2];
		if(containsPlayers) { 
			if(a.getName().equals("TestName2") && b.getName().equals("TestName3") && c.getName().equals("TestName1")) { 
				assertTrue(true);
			}
			else { 
				assertTrue(false, 
					"Expected names have not been set!\nExpected: TestName2, TestName3, TestName1 but was:\n " + a.getName() + ", " + b.getName() + ", " + c.getName());
			}
		}
		else { 
			assertTrue(false, "Board does not contain any player objects");
		}
	}
	
	@Test
	public void createBoardWithFlagPositions() { 
		char[][] flagBoard = {
				{'.','.','.','4','.','2'},
				{'.','.','.','.','.','.'},
				{'.','3','.','.','.','.'},
				{'.','.','1','.','.','.'},
				{'.','.','.','.','.','.'}
			};
		Game game = new Game(testNames, testMoves, flagBoard, null);
		boolean containsFlags = Stream.of(game.getBoard().getInitialState())
				.flatMap(Stream::of).anyMatch(i -> i instanceof Flag);
		
		Flag flag1 = (Flag) game.getBoard().getInitialState()[3][2];
		Flag flag2 = (Flag) game.getBoard().getInitialState()[0][5];
		Flag flag3 = (Flag) game.getBoard().getInitialState()[2][1];
		Flag flag4 = (Flag) game.getBoard().getInitialState()[0][3];
		if(containsFlags) { 
			if(flag1.getFlagValue() == 1 && flag2.getFlagValue() == 2 && flag3.getFlagValue() == 3 && flag4.getFlagValue() == 4) { 
				assertTrue(true);
			}
			else { 
				assertTrue(false, 
					"Expected flags have not been set!\nExpected: 1, 2, 3, 4 but was:\n " + flag1.getFlagValue() + ", " + flag2.getFlagValue() + ", " + flag1.getFlagValue() + ", " + flag4.getFlagValue());
			}
		}
		else { 
			assertTrue(false, "Board does not contain any flag objects");
		}
	}
	
	@Test
	public void createBoardWithGearPositions() { 
		char[][] gearBoard = {
				{'.','.','.','+','.','-'},
				{'.','.','.','.','.','.'},
				{'.','+','.','.','.','.'},
				{'.','.','-','.','.','.'},
				{'.','.','.','.','.','.'}
			};
		Game game = new Game(testNames, testMoves, gearBoard, null);
		boolean containsGears = Stream.of(game.getBoard().getInitialState())
				.flatMap(Stream::of).anyMatch(i -> i instanceof Gear);
		if(containsGears) { 
			assertTrue(true);
		}
		else { 
			assertTrue(false, "Board does not contain any Gear objects");
		}
	}
	
	@Test
	public void createBoardWithPitPositions() { 
		char[][] pitsBoard = {
				{'x','.','.','x','.','x'},
				{'.','.','.','.','.','.'},
				{'.','x','.','.','.','.'},
				{'.','.','x','.','.','.'},
				{'.','.','.','.','x','.'}
			};
		Game game = new Game(testNames, testMoves, pitsBoard, null);
		boolean containsPits = Stream.of(game.getBoard().getInitialState())
				.flatMap(Stream::of).anyMatch(i -> i instanceof Pit);
		if(containsPits) { 
			assertTrue(true);
		}
		else { 
			assertTrue(false, "Board does not contain any Pit objects");
		}
	}
	
	@Test
	public void ensureThatPlayerStartPositionIsSet() { 
		char[][] playerBoard = {
				{'.','.','.','.','.','.'},
				{'.','.','.','.','.','.'},
				{'.','.','.','.','.','.'},
				{'.','.','.','.','C','.'},
				{'.','.','.','.','.','.'}
			};
		Game game = new Game(testNames, testMoves, playerBoard, null);
		if(game.getBoard().getInitialState()[3][4] instanceof Player) { 
			String result = "";
			for(int position : ((Player)game.getBoard().getInitialState()[3][4]).getStartPosition()) { 
				result += position + " ";
			}
			assertEquals("3 4 ", result);
		}
		else { 
			assertTrue(false, "No Player object at [3][4]. Actual was " + game.getBoard().getInitialState()[3][4].getClass().getName());
		}
	}
	
	@Test
	public void ensureThatPlayerPositionIsSet() { 
		char[][] playerBoard = {
				{'.','.','.','.','.','.'},
				{'.','.','.','.','.','.'},
				{'.','A','.','.','.','.'},
				{'.','.','.','.','.','.'},
				{'.','.','.','.','.','.'}
			};
		Game game = new Game(testNames, testMoves, playerBoard, null);
		if(game.getBoard().getInitialState()[2][1] instanceof Player) { 
			String result = "";
			for(int position : ((Player)game.getBoard().getInitialState()[2][1]).getPosition()) { 
				result += position + " ";
			}
			assertEquals("2 1 ", result);
		}
		else { 
			assertTrue(false, "No Player object at [2][1]. Actual was " + game.getBoard().getInitialState()[2][1].getClass().getName());
		}
	}
}