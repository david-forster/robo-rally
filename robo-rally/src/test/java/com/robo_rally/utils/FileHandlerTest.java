package com.robo_rally.utils;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("FileHandler Tests")
class FileHandlerTest {
	
	String resourcePath = System.getProperty("user.dir") + "\\src\\test\\resources";

    @Test
    @DisplayName("Exception Test")
    public void exceptionTesting()	 {
    	FileHandler fh = new FileHandler("", "");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
    }
    
	@Test
    @DisplayName("Valid Board Test - Filled 8x5")
	public void readValidBoardFileEightByFive() {
		char[][] expectedBoardResult = {
				{'.','.','.','.','.','.','.','.'},
				{'.','.','.','1','.','.','.','.'},
				{'.','.','.','.','.','.','.','.'},
				{'.','.','A','.','.','B','.','.'},
				{'.','.','.','.','.','.','.','.'}
			};
		
    	FileHandler fh = new FileHandler(resourcePath + "\\games\\01-90degturn\\board.brd", "");
        assertArrayEquals(expectedBoardResult, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Valid Board Test - Filled 7x5 with pit")
	public void readValidBoardFileSevenByFiveWithPit() {
		char[][] expectedBoardResult = {
				{'.','.','.','.','.','.','.'},
				{'.','.','x','1','.','.','.'},
				{'.','.','.','.','2','.','.'},
				{'.','.','A','C','B','.','.'},
				{'.','.','.','.','.','.','.'}
			};
		
    	FileHandler fh = new FileHandler(resourcePath + "\\games\\02-pits\\board.brd", "");
        assertArrayEquals(expectedBoardResult, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
    
	@Test
    @DisplayName("Valid Board Test - Filled 8x5 with gears")
	public void readValidBoardFileEightByFiveWithGears() {
		char[][] expectedBoardResult = {
				{'.','.','.','.','.','.','.','.'},
				{'.','.','.','+','2','1','.','-'},
				{'.','.','.','.','.','.','.','.'},
				{'.','.','.','A','.','.','.','B'},
				{'.','.','.','.','.','.','.','.'}
			};
		
    	FileHandler fh = new FileHandler(resourcePath + "\\games\\03-gears\\board.brd", "");
        assertArrayEquals(expectedBoardResult, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Board Test - Filled 6x4 - No flags present")
	public void readInvalidBoardFileSixByFour() {
    	FileHandler fh = new FileHandler(resourcePath + "\\boards\\only-starting.brd", "");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Board Test - Empty 2x1 - No players, no flags")
	public void readInvalidBoardFileTwoByOne() {
    	FileHandler fh = new FileHandler(resourcePath + "\\boards\\empty-2x1.brd", "");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Board Test - Unknown Character")
	public void readInvalidBoardUnknownChar() {
    	FileHandler fh = new FileHandler(resourcePath + "\\boards\\invalid-character.brd", "");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Board Test - Empty Board")
	public void readInvalidBoardEmptyBoard() {
    	FileHandler fh = new FileHandler(resourcePath + "\\boards\\empty-0x0.brd", "");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Board Test - Mismatched Row Lengths")
	public void readInvalidBoardMismatchedRows() {
    	FileHandler fh = new FileHandler(resourcePath + "\\boards\\mismatched-rows-cols.brd", "");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Valid Moves Test - Single Round, 4 players")
	public void readValidMovesFileSingleRoundFourPlayers() {
		char[][][] expectedMovesResult = {
				{{'F','L','F','W','F'}}, //Alice moves
				{{'W','F','W','F','L'}}, //Bob moves
				{{'R','F','W','F','L'}}, //Charlie moves
				{{'F','R','W','R','F'}}  //Doug moves
			};
		String[] expectedNames = {"Alice", "Bob", "Charlie", "Doug"};
		
		
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\4players.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(expectedMovesResult, fh.getMoves());
        assertArrayEquals(expectedNames, fh.getNames());
	}
	
	@Test
    @DisplayName("Valid Moves Test - 2 Rounds, 2 players")
	public void readValidMovesFileTwoRoundsTwoPlayers() {
		char[][][] expectedMovesResult = {
				{{'F','L','F','W','F'}, {'R','F','W','F','L'}}, //Alice moves
				{{'W','F','W','U','L'}, {'F','R','B','R','F'}} //Bob moves
			};
		String[] expectedNames = {"Alice", "Bob"};
		
		
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\2players-2rounds.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(expectedMovesResult, fh.getMoves());
        assertArrayEquals(expectedNames, fh.getNames());
	}

	@Test
	public void readValidMovesFilePlayerNameMaxLimit() {
		char[][][] expectedMovesResult = {
				{{'F','L','F','W','F'}}, //Alice moves
				{{'W','F','W','F','L'}} //Bob moves
			};
		String[] expectedNames = {"Aliceawdawdawdawdawd", "Bob"};
		
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\2players-max-length-name.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(expectedMovesResult, fh.getMoves());
        assertArrayEquals(expectedNames, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Moves Test - Empty File")
	public void readInvalidMovesFileEmptyFile() {
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\empty-file.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
	public void readInvalidMovesFileTooManyPlayers() {
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\5players.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
	public void readInvalidMovesFilePlayerNameTooLong() {
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\2players-name-too-long.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Moves Test - Not Enough Moves")
	public void readInvalidMovesFileNotEnoughMoves() {
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\1player-not_enough.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Moves Test - Not Enough Moves")
	public void readInvalidMovesFileRepeatedMoves() {
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\1player-repeated.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
	
	@Test
    @DisplayName("Invalid Moves Test - Too Many Programs")
	public void readInvalidMovesFileTooManyPrograms() {
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\2players-too-many-programs.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}

	@Test
    @DisplayName("Invalid Moves Test - Not Enough Programs")
	public void readInvalidMovesFileNotEnoughPrograms() {
    	FileHandler fh = new FileHandler("", resourcePath + "\\programs\\2players-missing-program.prg");
        assertArrayEquals(null, fh.getBoard());
        assertArrayEquals(null, fh.getMoves());
        assertArrayEquals(null, fh.getNames());
	}
}