package com.robo_rally.utils;

import javafx.application.Application;
import javafx.stage.Stage;

public class TestHooks extends Application {

	private static Thread thread;

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub

	}

	public static void initiateThread() {
		if (thread == null) {
			thread = new Thread("Initiating JFX thread.....") {
				public void run() {
					Application.launch(TestHooks.class, new String[0]);
				}
			};
			thread.setDaemon(true);
			thread.start();
			try {
				/*
				 * Needed to add this otherwise it threw an exception Slows down test execution
				 * by 1s * number_of_test_classes
				 */
				thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
