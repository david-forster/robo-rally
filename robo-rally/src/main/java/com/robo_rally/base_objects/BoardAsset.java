package com.robo_rally.base_objects;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public abstract class BoardAsset {
	
	@FXML
	private Label lbl_Content = new Label();

	public BoardAsset(String content) {
		lbl_Content.setText(content);
		lbl_Content.setFont(new Font("System", 14));
		lbl_Content.setMinWidth(30);
		lbl_Content.setMinHeight(30);
		lbl_Content.setPrefWidth(30);
		lbl_Content.setPrefHeight(30);
		lbl_Content.setAlignment(Pos.CENTER);
		lbl_Content.setTextAlignment(TextAlignment.CENTER);
	}
	

	public Label getLabel() {
		return this.lbl_Content;
	}
}