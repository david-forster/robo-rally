package com.robo_rally.base_objects;

import com.robo_rally.board_objects.Player;

public interface PlayerManipulator {
	
	public String action(Player player);

}