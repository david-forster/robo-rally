package com.robo_rally.view_controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.robo_rally.base_objects.BoardAsset;
import com.robo_rally.board_objects.Player;
import com.robo_rally.game_data.Game;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class BoardController extends Application {

	private BoardAsset[][] boardData;
	private HashMap<Character, Player> playerMapping;
	private Game gameObject;
	private int roundNum = 1;
	
	private Timer btnDisable;

	@FXML
	private GridPane grpn_Board;

	@FXML
	private Label lbl_PlayerA;

	@FXML
	private Label lbl_PlayerB;

	@FXML
	private Label lbl_PlayerC;

	@FXML
	private Label lbl_PlayerD;

	@FXML
	private Label lbl_Round;

	@FXML
	private Button btn_Start;
	
	@FXML
	private Text lbl_SystemLog;

	public BoardController(BoardAsset[][] boardData, HashMap<Character, Player> playerMapping, Game game) {
		this.boardData = boardData;
		this.playerMapping = playerMapping;
		this.gameObject = game;
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			// 1. Load FXML for main window
			FXMLLoader l = new FXMLLoader();
			l.setLocation(getClass().getResource("/com/robo_rally/UI/Board.fxml"));
			l.setController(this);
			HBox root = l.load();

			grpn_Board = new GridPane();

			final int amountOfRows = this.boardData.length;
			final int amountOfCols = this.boardData[0].length;

			grpn_Board.setGridLinesVisible(true);
			grpn_Board.setAlignment(Pos.CENTER);
			ColumnConstraints[] cols = new ColumnConstraints[amountOfCols];

			ColumnConstraints colStyle = new ColumnConstraints();
			colStyle.setPercentWidth(100 / amountOfCols);
			colStyle.setMinWidth(30);
			colStyle.setPrefWidth(30);

			Arrays.fill(cols, colStyle);

			RowConstraints[] rows = new RowConstraints[amountOfRows];

			RowConstraints rowStyle = new RowConstraints();
			rowStyle.setPercentHeight(100 / amountOfRows);
			rowStyle.setMinHeight(30);
			rowStyle.setPrefHeight(30);

			Arrays.fill(rows, rowStyle);

			grpn_Board.getColumnConstraints().addAll(cols);
			grpn_Board.getRowConstraints().addAll(rows);

			this.renderGrid(this.boardData);

			VBox vb = (VBox) root.getChildren().get(0);
			vb.getChildren().add(2, grpn_Board);

			// 2. Set up Scene
			Scene scene = new Scene(root);
			// 3. Set up primaryStage and show it
			primaryStage.setTitle("Robo Rally - Game");
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.centerOnScreen();

			double labelSizing = (primaryStage.getWidth() - 20) / 2; // panel size minus padding, halved

			if (playerMapping.get('A') != null) {
				lbl_PlayerA.setText(lbl_PlayerA.getText() + playerMapping.get('A').getName());
				lbl_PlayerA.setPrefWidth(labelSizing);
			} else {
				lbl_PlayerA.setVisible(false);
			}

			if (playerMapping.get('B') != null) {
				lbl_PlayerB.setText(lbl_PlayerB.getText() + playerMapping.get('B').getName());
				lbl_PlayerB.setPrefWidth(labelSizing);
			} else {
				lbl_PlayerB.setVisible(false);
			}

			if (playerMapping.get('C') != null) {
				lbl_PlayerC.setText(lbl_PlayerC.getText() + playerMapping.get('C').getName());
				lbl_PlayerC.setPrefWidth(labelSizing);
			} else {
				lbl_PlayerC.setVisible(false);
			}

			if (playerMapping.get('D') != null) {
				lbl_PlayerD.setText(lbl_PlayerD.getText() + playerMapping.get('D').getName());
				lbl_PlayerD.setPrefWidth(labelSizing);
			} else {
				lbl_PlayerD.setVisible(false);
			}

			lbl_Round.setPrefWidth(primaryStage.getWidth());
			lbl_Round.setText("Round: 1");
			
			lbl_SystemLog.setText("");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void renderGrid(BoardAsset[][] dataToRender) {
		grpn_Board.getChildren().remove(1, grpn_Board.getChildren().size()); // removes all children except formatting
		for (int row = 0; row < dataToRender.length; row++) {
			for (int col = 0; col < dataToRender[0].length; col++) {
				Node asset = dataToRender[row][col] != null ? dataToRender[row][col].getLabel() : new Pane();

				grpn_Board.add(asset, col, row);
				GridPane.setRowIndex(asset, row);
				GridPane.setColumnIndex(asset, col);
			}
		}
	}

	/**
	 * Initializes the controller class. This method is automatically called after
	 * the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		btn_Start.setOnAction((evt) -> {
			updateSystemLog("Round " + roundNum + " started");
			this.gameObject.start(roundNum);
			btn_Start.setDisable(true);

			lbl_Round.setText("Round: " + String.valueOf(roundNum));

			Date triggerTime = new Date();
			
			//1 sec per move, each player does 5 moves -> (playerMapping.keySet().size() * 5000)
			//1 sec per board activate, with 0.5 sec delay to prevent button spamming -> + 5500
			triggerTime.setTime(triggerTime.getTime() + (playerMapping.keySet().size() * 5000) + 5500); 
			this.btnDisable = new Timer();
			this.btnDisable.schedule(new TimerTask() {
				@Override
				public void run() {
					btn_Start.setDisable(false);
					roundNum++;
				}
			}, triggerTime);
		});
	}

	public void gameWon() {
		this.btnDisable.cancel();
		btn_Start.setDisable(true);
		updateSystemLog("Game Over!");
	}
	
	public void updateSystemLog(String update) {
		if (update != null) {
			lbl_SystemLog.setText(lbl_SystemLog.getText() + " - " + update + "\n");
		}
	}
}