package com.robo_rally.view_controllers;

import java.io.File;
import java.io.IOException;

import com.robo_rally.utils.FileHandler;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Menu extends Application {

	Stage primaryStage;
	
	public static void main(String[] args) {
		launch();
	}
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		try {
			// 1. Load FXML for main window
			FXMLLoader l = new FXMLLoader();
			l.setLocation(getClass().getResource("/com/robo_rally/UI/Menu.fxml"));
			l.setController(this);
			VBox root = l.load();
			// 2. Set up Scene
			Scene scene = new Scene(root);
			// 3. Set up primaryStage and show it
			primaryStage.setTitle("Robo Rally - Menu");
			primaryStage.setScene(scene);
			
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			    @Override
			    public void handle(WindowEvent t) {
			        Platform.exit();
			        System.exit(0);
			    }
			});
			
			primaryStage.show();
			primaryStage.centerOnScreen();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	Label lbl_BoardHeader;

	@FXML
	Label lbl_BoardPath;
	
	@FXML
	Button btn_BoardUpload;
	
	@FXML
	Label lbl_MovesHeader;
	
	@FXML
	Label lbl_MovesPath;

	@FXML
	Button btn_MoveUpload;
	
	@FXML
	Button btn_Continue;
	
	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		btn_BoardUpload.setOnAction((evt) -> {
			 FileChooser fileChooser = new FileChooser();
			 fileChooser.setTitle("Select Board File");
			 fileChooser.getExtensionFilters().addAll(
			         new ExtensionFilter("Board Files", "*.brd"),
			         new ExtensionFilter("All Files", "*.*"));
			 fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
			 File selectedBoardFile = fileChooser.showOpenDialog(this.primaryStage);
			 if (selectedBoardFile != null) {
				lbl_BoardPath.setText(selectedBoardFile.getPath());
				if (lbl_MovesPath.getText() != "") 
					btn_Continue.setDisable(false);
			 }
		});
		
		btn_MoveUpload.setOnAction((evt) -> {
			 FileChooser fileChooser = new FileChooser();
			 fileChooser.setTitle("Select Program File");
			 fileChooser.getExtensionFilters().addAll(
			         new ExtensionFilter("Program Files", "*.prg"),
			         new ExtensionFilter("All Files", "*.*"));
			 File selectedMovesFile = fileChooser.showOpenDialog(this.primaryStage);
			 if (selectedMovesFile != null) {
				lbl_MovesPath.setText(selectedMovesFile.getPath());
				if (lbl_BoardPath.getText() != "") 
					btn_Continue.setDisable(false);
			 }
		});
		
		btn_Continue.setOnAction((evt) -> {
			File boardFile = new File(lbl_BoardPath.getText());
			File movesFile = new File(lbl_MovesPath.getText());
			
			if (boardFile.isFile() && movesFile.isFile()) {
				lbl_BoardHeader.setTextFill(Color.BLACK);
				lbl_BoardPath.setTextFill(Color.BLACK);
				lbl_BoardPath.setStyle("-fx-border-color: black;");
				
				lbl_MovesHeader.setTextFill(Color.BLACK);
				lbl_MovesPath.setTextFill(Color.BLACK);
				lbl_MovesPath.setStyle("-fx-border-color: black;");
				
				FileHandler fh = new FileHandler(lbl_BoardPath.getText(), lbl_MovesPath.getText());
				fh.createGame(this.primaryStage);
				
				if (fh.getBoard() == null) {
					lbl_BoardHeader.setTextFill(Color.RED);
					lbl_BoardPath.setTextFill(Color.RED);
					lbl_BoardPath.setStyle("-fx-border-color: red;");
				} else {
					lbl_BoardHeader.setTextFill(Color.BLACK);
					lbl_BoardPath.setTextFill(Color.BLACK);
					lbl_BoardPath.setStyle("-fx-border-color: black;");
				}
				
				if (fh.getMoves() == null || fh.getNames() == null) {
					lbl_MovesHeader.setTextFill(Color.RED);
					lbl_MovesPath.setTextFill(Color.RED);
					lbl_MovesPath.setStyle("-fx-border-color: red;");
				} else {
					lbl_MovesHeader.setTextFill(Color.BLACK);
					lbl_MovesPath.setTextFill(Color.BLACK);
					lbl_MovesPath.setStyle("-fx-border-color: black;");
				}
				
			} else {
				if (!boardFile.isFile()) {
					lbl_BoardHeader.setTextFill(Color.RED);
					lbl_BoardPath.setTextFill(Color.RED);
					lbl_BoardPath.setStyle("-fx-border-color: red;");
				} else {
					lbl_BoardHeader.setTextFill(Color.BLACK);
					lbl_BoardPath.setTextFill(Color.BLACK);
					lbl_BoardPath.setStyle("-fx-border-color: black;");
				}
				
				if (!movesFile.isFile()) {
					lbl_MovesHeader.setTextFill(Color.RED);
					lbl_MovesPath.setTextFill(Color.RED);
					lbl_MovesPath.setStyle("-fx-border-color: red;");
				} else {
					lbl_MovesHeader.setTextFill(Color.BLACK);
					lbl_MovesPath.setTextFill(Color.BLACK);
					lbl_MovesPath.setStyle("-fx-border-color: black;");
				}

				Alert alert = new Alert(AlertType.ERROR, "A provided file does not exist, please select another one", ButtonType.OK);
				alert.showAndWait();
			}
		});
	}
}