package com.robo_rally.board_objects;

import com.robo_rally.base_objects.BoardAsset;
import com.robo_rally.base_objects.PlayerManipulator;

//abstraction as doesn't need an action method
public class Flag extends BoardAsset implements PlayerManipulator {
	//position variable available from BoardAsset
	private final int FLAG_NUMBER;
	
	public Flag(int flagNumber) {
		super(String.valueOf(flagNumber));
		if(flagNumber < 1 || flagNumber > 4) {
			throw new IllegalArgumentException("The flag must have a value between 1 and 4. Passed flag has a value of: " + flagNumber);
		}
		FLAG_NUMBER = flagNumber;
		
	}

	public int getFlagValue() {
		return this.FLAG_NUMBER;
	}

	@Override
	public String action(Player player) {
		if (this.FLAG_NUMBER == player.getLastCapturedFlag() + 1) {
			player.setLastCapturedFlag(this.FLAG_NUMBER);
			return player.getName() + " captured flag #" + String.valueOf(this.FLAG_NUMBER);
		}
		return null;
	}
}