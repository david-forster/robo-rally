package com.robo_rally.board_objects;

public enum MoveDirections {
	
	F('-', '+', '+', '-'),
	B('+', '-', '-', '+'),
	L('A', 'A', 'A', 'A'),
	R('C', 'C', 'C', 'C'),
	//H = half (rotation)
	U('H', 'H', 'H', 'H'),
	W('.', '.', '.', '.');
	
	private final char north;
	private final char east;
	private final char south;
	private final char west;
	
	private MoveDirections(char north, char east, char south, char west) {
		this.north = north;
		this.east = east;
		this.south = south;
		this.west = west;
	}
	
	public char getNorth() { 
		return north;
	}
	
	public char getEast() { 
		return east;
	}
	
	public char getSouth() { 
		return south;
	}
	
	public char getWest() { 
		return west;
	}
}