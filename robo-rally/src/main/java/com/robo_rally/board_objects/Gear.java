package com.robo_rally.board_objects;

import com.robo_rally.base_objects.BoardAsset;
import com.robo_rally.base_objects.PlayerManipulator;

public class Gear extends BoardAsset implements PlayerManipulator {

	private char direction; // either A for anti-clockwise or C for clockwise

	public Gear(char direction) {
		super(String.valueOf(direction));
		if (direction == '+') {
			this.direction = 'C';
		} else if (direction == '-') {
			this.direction = 'A';
		} else {
			throw new IllegalArgumentException(
					"Could not create a gear using the character: " + direction + ". Direction must be one of: + or -");
		}
	}

	@Override
	public String action(Player player) {
		try { 
			player.rotate(direction);
			
			return player.getName() + " was rotated " + (this.direction == 'C' ? "clockwise" : "anticlockwise");
		}
		catch(IllegalArgumentException iae) { 
			System.err.println(iae);
			return null;
		}
	}
}
