package com.robo_rally.board_objects;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import com.robo_rally.base_objects.BoardAsset;

import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Player extends BoardAsset {

	private String name;
	private int[] position;
	private int[] startPosition;
	private char direction;
	private char[][] listOfMoves;
	private int lastCapturedFlag;
	private Queue<Character> moveQueue = new LinkedList<Character>();
	private Map<Character, char[]> directionMap = new HashMap<Character, char[]>(Map.of(
			'N', new char[] { 'W', 'E' },
			'E', new char[] { 'N', 'S' }, 
			'S', new char[] { 'E', 'W' }, 
			'W', new char[] { 'S', 'N' }));
	
	private Map<Character, String> loggingParser = new HashMap<Character, String>(Map.of(
			'N', "north",
			'E', "east",
			'S', "south", 
			'W', "west",
			'F', " moved forward while facing ",
			'B', " moved backward while facing ",
			'L', " turned left to face ",
			'R', " turned right to face ",
			'P', " waited",
			'U', " performed a U turn"
		));

	public Player(String name, char[][] listOfMoves, char symbol) {
		super(String.valueOf(symbol));
		this.name = name;
		this.listOfMoves = listOfMoves;
		this.direction = 'N';
		this.lastCapturedFlag = 0;
		this.getLabel().setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/com/robo_rally/Graphics/N Template.png"))));
		this.getLabel().setContentDisplay(ContentDisplay.CENTER);

		// Might need to have a look at making this account for rounds
		for (char[] array : this.listOfMoves) {
			for (char move : array) {
				moveQueue.add(move);
			}
		}
	}


	public String getName() {
		return this.name;
	}

	public String action() {
		if (!moveQueue.isEmpty()) {
			char move = moveQueue.poll();
			movePlayer(move, this.direction); 
			
			if (move == 'W' || move == 'U') {
				move = move == 'W' ? 'P' : move; //avoids duplicate key issue
				return this.getName() + loggingParser.get(move);
			} else {
				return this.getName() + loggingParser.get(move) + loggingParser.get(this.direction);
			}
		} else {
			System.out.printf("%s has no more remaining moves%n", name);
			return this.getName() + " has run out of moves";
		}
	}

	/*
	 * I have added a queue structure where we can store the player moves in an
	 * ordered manner. When the player makes a move using action(), we will pop the
	 * first move from the queue and request a value from an enum; since N, E, S, W
	 * can have different actions. We then use this to update the player's position
	 * or direction
	 */
	public void movePlayer(char move, char direction) {

			MoveDirections md = MoveDirections.valueOf(Character.toString(move));
			char moveOperation;
			int[] position = Arrays.copyOf(getPosition(), 2);

			if (direction == 'N') {
				moveOperation = md.getNorth();
				position[0] = performMoveOperation(moveOperation, position[0]);
			} else if (direction == 'E') {
				moveOperation = md.getEast();
				position[1] = performMoveOperation(moveOperation, position[1]);
			} else if (direction == 'S') {
				moveOperation = md.getSouth();
				position[0] = performMoveOperation(moveOperation, position[0]);
			} else if (direction == 'W') {
				moveOperation = md.getWest();
				position[1] = performMoveOperation(moveOperation, position[1]);
			} else {
				throw new IllegalArgumentException(direction + " is not a valid direction of: N, E, S, W");
			}
			
			setPosition(position);
		} 

	private int performMoveOperation(char moveOperation, int arrayLocation) {
		switch (moveOperation) {
		case '+':
			arrayLocation += 1;
			break;
		case '-':
			arrayLocation -= 1;
			break;
		case 'H':
			rotate('A');
			rotate('A');
			break;
		case 'A':
			rotate(moveOperation);
			break;
		case 'C':
			rotate(moveOperation);
			break;
		case '.':
			// Wait and do nothing
			break;
		default:
			throw new IllegalArgumentException(
					"Player move passed was: " + moveOperation + ". Move must be one of: F, B, L, R, U, W");
		}
		return arrayLocation;
	}

	/*
	 * Because player does not interact with the board, when the respawn function is
	 * called we will just set it. Then, when the board is about to re-render, we
	 * will check the N, E, S, W nodes
	 */
	public void respawn() {
		this.setPosition(this.startPosition);
		this.setDirection('N');
	}

	public void rotate(char rotation) {
		char currentDirection = getDirection();
		if (rotation == 'A') {
			setDirection(directionMap.get(currentDirection)[0]);
		} else if (rotation == 'C') {
			setDirection(directionMap.get(currentDirection)[1]);
		} else {
			throw new IllegalArgumentException(
					rotation + " is not a valid direction. Direction must be A (anti-clockwise) or C (clockwise)");
		}
	}

	public int getLastCapturedFlag() {
		return this.lastCapturedFlag;
	}

	public void setLastCapturedFlag(int newFlag) {
		if(newFlag >= 1 && newFlag <= 4) {
			this.lastCapturedFlag = newFlag;
		}
		else { 
			throw new IllegalArgumentException("Invalid flag value: " + newFlag + ". Flag must be between 1 and 4");
		}
	}
	
	public int[] getStartPosition() { 
		return this.startPosition;
	}
	
	public void setStartPosition(int[] startPosition) { 
		this.startPosition = startPosition;
	}
	
	public char getDirection() {
		return this.direction;
	}
	
	public char[][] getMoves() { 
		return this.listOfMoves;
	}
	
	public int[] getPosition() {
		return this.position;
	}
	
	public void setPosition(int[] position) { 
		this.position = position;
	}

	public void setDirection(char direction) {
		if(direction == 'N' || direction == 'E' || direction == 'S' || direction == 'W') {
			this.direction = direction;
			this.getLabel().setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/com/robo_rally/Graphics/" + String.valueOf(direction) + " Template.png"))));
		}
		else { 
			throw new IllegalArgumentException(direction + " is not a valid direction of: N, E, S, W");
		}
	}
}