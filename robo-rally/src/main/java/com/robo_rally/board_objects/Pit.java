package com.robo_rally.board_objects;

import com.robo_rally.base_objects.BoardAsset;
import com.robo_rally.base_objects.PlayerManipulator;

public class Pit extends BoardAsset implements PlayerManipulator {

	public Pit() {
		super("X");
	}

	@Override
	public String action(Player player) {
		player.respawn(); //moves player back to start
		return player.getName() + " fell down a pit!";
	}
}