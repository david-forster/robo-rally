package com.robo_rally.utils;

import com.robo_rally.view_controllers.Menu;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

public class AlertMessage {

	public AlertMessage() {
	}

	public static void display(String message) {
		Alert alert = new Alert(AlertType.ERROR, message, ButtonType.OK);
		alert.showAndWait();
	}

	public static void displayAndMenu(String message, Stage primaryStage) {
		Alert alert = new Alert(AlertType.NONE, message, ButtonType.OK);
        Platform.runLater(() -> {
        	alert.showAndWait();
        	
    		if (alert.getResult() == ButtonType.OK) {
    			Menu mn = new Menu();
    			mn.start(primaryStage);
    		} 
        });
	}
}