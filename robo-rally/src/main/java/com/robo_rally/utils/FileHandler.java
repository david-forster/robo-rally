package com.robo_rally.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.robo_rally.game_data.Game;

import javafx.stage.Stage;

public class FileHandler {
	char[][] board;
	char[][][] moves;
	String[] names;
	
	public FileHandler(String boardPath, String movesPath) { 
		board = boardReader(boardPath);
		moves = movesReader(movesPath);
	}
	
	
	public void createGame(Stage primaryStage) {
		if (this.names == null && this.moves != null) {
			AlertMessage.display("Please provide a username less than 20 characters long");
		} else if (this.names == null && this.moves == null) {
			AlertMessage.display("Please provide a valid program file");
		} else if (board == null) {
			AlertMessage.display("Please make sure that the board is configured with a valid file format");
		} else {
			new Game(this.names, this.moves, this.board, primaryStage);
		}
	}
	
	/*
	 * @param   path   String variable relating to location of file
	 * @return   char[][]   char[][] parsed from a ArrayList<char[]>
	 */
	private char[][] boardReader(String path) {
		List<char[]> lines = new ArrayList<char[]>();
		try (BufferedReader buffRead = new BufferedReader(new FileReader(path))) {
		    String line;

		    Pattern linePattern = Pattern.compile("^[a-dA-D1-4+\\-\\.xX]+$", Pattern.MULTILINE);
		    Pattern playerPattern = Pattern.compile("[a-dA-D]+", Pattern.MULTILINE);
		    Pattern flagPattern = Pattern.compile("[1-4]+", Pattern.MULTILINE);
		    
		    boolean[] fileValid = {true, false, false}; //line lengths, contains player, contains flag
		    int lineLength = 0;
		    while((line = buffRead.readLine()) != null) {
		    	boolean lineMatch = linePattern.matcher(line).find();
		    	boolean playerMatch = playerPattern.matcher(line).find();
		    	boolean flagMatch = flagPattern.matcher(line).find();
		    	
		    	if (!line.contains("format") && lineMatch) { //ignores version of the board file
		    		if (lineLength == 0) {
		    			lineLength = line.length();
		    		} else if (line.length() != lineLength) {
		    			fileValid[0] = false;
		    		}
		    		
		    		lines.add(line.toCharArray()); //Each line broken into a Char Array and added for easier processing
		    		fileValid[1] = playerMatch ? true : fileValid[1]; //returns true if at least one player present
		    		fileValid[2] = flagMatch ? true : fileValid[2]; //returns true if at least one flag present
		    	} else if (!line.contains("format") && !lineMatch) {
		    		throw new IOException("Invalid file format");
		    	}
		    }
		    buffRead.close();
		    
		    if (lines.size() < 1) {
	    		throw new IOException("Empty file provided");
		    }
		    
		    if (fileValid[0] == false || fileValid[1] == false || fileValid[2] == false) {
	    		throw new IOException("Invalid file format");
		    }
		} catch (IOException ie) {
			System.err.println(ie);
			return null; //sets board to null
		}
		
	    return lines.toArray(new char[][]{}); //convert ArrayList into char[][]
	}
	
	/*
	 * @param   path   String variable relating to location of file
	 * @return   char[][][]   char[][][] parsed from a ArrayList<ArrayList<char[]>> stores all players moves per round
	 */
	private char[][][] movesReader(String path) {
		List<ArrayList<char[]>> lines = new ArrayList<ArrayList<char[]>>();
		
		try (BufferedReader buffRead = new BufferedReader(new FileReader(path))) {
		    String line;
		    
		    Pattern linePattern = Pattern.compile("^([bflruw]{5}[ ]*){1,4}$", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		    Pattern duplicateCharacterPattern = Pattern.compile("[^\\w\\s]|(.)\\1", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

		    boolean[] fileValid = {true, true}; //same line lengths, duplicate characters
		    int lineLength = 0;
		    int lineNumber = 0;
		    while((line = buffRead.readLine()) != null) {
		    	boolean lineMatch = linePattern.matcher(line).find();
		    	boolean dupCharMatch = duplicateCharacterPattern.matcher(line).find();
		    	
		    	if (!line.contains("format") && (lineMatch || lineNumber == 1)) { //ignores formatting version line
		    		if (lineNumber == 1) {
			    		this.names = line.split(" ");
			    		
			    		if (this.names.length > 4)
				    		throw new IOException("Too many names provided");
			    		
			    		for (int i = 0; i < this.names.length; i++) {
			    			if (this.names[i].length() > 20)
					    		throw new IOException("Provided file includes a name that exceeds constraints");
			    			
			    			lines.add(new ArrayList<char[]>());
			    		}
			    		
			    		lineLength = (5 * names.length) + (names.length-1); //5 moves per player, with a space between each instruction, none following or proceeding
		    		} else {
		    			if (line.trim().length() != lineLength) {
			    			fileValid[0] = false;
			    		} else {
			    			String[] splitLine = line.split(" ");
			    			
			    			for (int i = 0; i < this.names.length; i++) {
			    				ArrayList<char[]> currList = lines.get(i);
			    				currList.add(splitLine[i].toCharArray());
			    			}
			    		}
		    		}
		    		
		    		fileValid[1] = dupCharMatch ? false : fileValid[1]; //returns false if duplicate move has been submitted (e.g. WFFRL)
		    	} else if (!line.contains("format") && !lineMatch && lineNumber != 1) {
		    		throw new IOException("Invalid file format");
		    	}

		    	
		    	lineNumber++;
		    }
		    
		    if (lines.size() < 1) {
	    		throw new IOException("Empty file provided");
		    }
		    
		    if (fileValid[0] == false || fileValid[1] == false) {
	    		throw new IOException("Invalid file format");
		    }
		    buffRead.close();
		} catch (IOException ie) {
			System.err.println(ie);
			this.names = null;
			return null; //sets moves to null
		}
		
		char[][][] returnHelper = new char[lines.size()][][];
		
		for (int i = 0; i < lines.size(); i++) {
			returnHelper[i] = lines.get(i).toArray(new char[][]{});
		}

	    return returnHelper;
	}
	
	//used for testing purposes
	public char[][] getBoard() {
		return this.board;
	}
	
	//used for testing purposes
	public char[][][] getMoves() {
		return this.moves;
	}
	
	//used for testing purposes
	public String[] getNames() {
		return this.names;
	}
}