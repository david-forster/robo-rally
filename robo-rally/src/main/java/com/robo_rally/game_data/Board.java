package com.robo_rally.game_data;

import java.util.HashMap;

import com.robo_rally.base_objects.BoardAsset;
import com.robo_rally.base_objects.PlayerManipulator;
import com.robo_rally.board_objects.Flag;
import com.robo_rally.board_objects.Pit;
import com.robo_rally.board_objects.Player;
import com.robo_rally.view_controllers.BoardController;

import javafx.stage.Stage;

public class Board {

	private final BoardAsset[][] INITIAL_STATE;
	private BoardAsset[][] currentState;
	private final BoardController uiController;
	private final int maxFlag;

	// Constructor for use with testing
	public Board(BoardAsset[][] initialBoard, BoardAsset[][] currentBoard, int maxFlag) {
		this.INITIAL_STATE = initialBoard;
		this.currentState = currentBoard;
		this.uiController = null;
		this.maxFlag = maxFlag;
	}

	public Board(BoardAsset[][] initialBoard, BoardAsset[][] currentBoard, HashMap<Character, Player> playerMapping,
			int maxFlag, Game game, Stage primaryStage) {
		this.INITIAL_STATE = initialBoard;
		this.currentState = currentBoard;
		this.uiController = new BoardController(this.currentState, playerMapping, game);
		this.maxFlag = maxFlag;

		if (primaryStage != null)
			this.uiController.start(primaryStage);
	}

	public int[] findFreeRespawnPoint(Player player) {
		int[] intendedRespawn = player.getStartPosition(); // default position
		if (this.currentState[intendedRespawn[0]][intendedRespawn[1]] == null || this.currentState[intendedRespawn[0]][intendedRespawn[1]] == player) {
			player.setPosition(intendedRespawn);
			return intendedRespawn;
		}

		intendedRespawn[0] -= 1; // north alternative

		if (intendedRespawn[0] > -1 && this.currentState[intendedRespawn[0]][intendedRespawn[1]] == null || this.currentState[intendedRespawn[0]][intendedRespawn[1]] == player) {
			player.setPosition(intendedRespawn);
			return intendedRespawn;
		}

		intendedRespawn[0] += 1; // east alternative
		intendedRespawn[1] += 1; // east alternative

		if (intendedRespawn[1] < this.currentState[0].length
				&& this.currentState[intendedRespawn[0]][intendedRespawn[1]] == null || this.currentState[intendedRespawn[0]][intendedRespawn[1]] == player) {
			player.setPosition(intendedRespawn);
			return intendedRespawn;
		}

		intendedRespawn[0] += 1; // south alternative
		intendedRespawn[1] -= 1; // south alternative

		if (intendedRespawn[0] < this.currentState.length
				&& this.currentState[intendedRespawn[0]][intendedRespawn[1]] == null || this.currentState[intendedRespawn[0]][intendedRespawn[1]] == player) {
			player.setPosition(intendedRespawn);
			return intendedRespawn;
		}

		intendedRespawn[0] -= 1; // west alternative
		intendedRespawn[1] -= 1; // west alternative

		if (intendedRespawn[1] > -1 && this.currentState[intendedRespawn[0]][intendedRespawn[1]] == null || this.currentState[intendedRespawn[0]][intendedRespawn[1]] == player) {
			player.setPosition(intendedRespawn);
			return intendedRespawn;
		}
		return player.getStartPosition();
	}

	public void collisionHandler(Player player, int[] oldPosition) {
		if (player.getPosition()[0] != oldPosition[0] || player.getPosition()[1] != oldPosition[1]) {
			playerChangeCell(player, oldPosition);
		}
		this.uiController.renderGrid(this.currentState);
	}

	private void playerChangeCell(Player player, int[] oldPosition) {
		int[] curPos = player.getPosition();
		if (curPos[0] < 0 || curPos[0] == this.currentState.length || curPos[1] < 0 || curPos[1] == this.currentState[0].length) {
			this.updateLogging(player.getName() + "went out of bounds!");
			player.respawn();
			int[] respPoint = this.findFreeRespawnPoint(player);
			this.currentState[respPoint[0]][respPoint[1]] = player;
			this.currentState[oldPosition[0]][oldPosition[1]] = (this.INITIAL_STATE[oldPosition[0]][oldPosition[1]] instanceof Player && this.INITIAL_STATE[oldPosition[0]][oldPosition[1]] != player)
					? null : this.INITIAL_STATE[oldPosition[0]][oldPosition[1]];
		} else if (this.currentState[player.getPosition()[0]][player.getPosition()[1]] instanceof Player) {
			Player collidedPlayer = (Player) this.currentState[player.getPosition()[0]][player.getPosition()[1]];

			this.updateLogging(player.getName() + " collided with " + collidedPlayer.getName());
			
			this.currentState[player.getPosition()[0]][player.getPosition()[1]] = player;

			collidedPlayer.movePlayer('F', player.getDirection());
			playerChangeCell(collidedPlayer, player.getPosition());
			
			this.currentState[player.getPosition()[0]][player.getPosition()[1]] = player;
			this.currentState[oldPosition[0]][oldPosition[1]] = this.INITIAL_STATE[oldPosition[0]][oldPosition[1]] instanceof Player
					? null : this.INITIAL_STATE[oldPosition[0]][oldPosition[1]];
		} else {
			this.currentState[player.getPosition()[0]][player.getPosition()[1]] = player;
			this.currentState[oldPosition[0]][oldPosition[1]] = this.INITIAL_STATE[oldPosition[0]][oldPosition[1]] instanceof Player
					? null : this.INITIAL_STATE[oldPosition[0]][oldPosition[1]];
		}
	}

	public String activate() {
		for (int row = 0; row < this.currentState.length; row++) {
			for (int col = 0; col < this.currentState[0].length; col++) {
				if (this.currentState[row][col] instanceof Player && this.INITIAL_STATE[row][col] instanceof PlayerManipulator) { // if player landed on flag, pit or gear

					this.updateLogging(((PlayerManipulator) this.INITIAL_STATE[row][col]).action((Player) this.currentState[row][col]));

					if (this.INITIAL_STATE[row][col] instanceof Pit) {
						int[] respPoint = this.findFreeRespawnPoint((Player) this.currentState[row][col]);
						this.currentState[respPoint[0]][respPoint[1]] = this.currentState[row][col];
						this.currentState[row][col] = this.INITIAL_STATE[row][col];
					}
					
					if (this.INITIAL_STATE[row][col] instanceof Flag && ((Player) this.currentState[row][col]).getLastCapturedFlag() == this.maxFlag) {
						return ((Player) this.currentState[row][col]).getName();
					}
					
					if (this.uiController != null) {
						this.uiController.renderGrid(this.currentState);
					}
				}
			}
		}
		return null;
	}
	
	//Used for controlling UI
	public void updateLogging(String update) {
		if (this.uiController != null) {
			this.uiController.updateSystemLog(update);
		}
	}
	
	//Used for controlling UI
	public void gameEnded() {
		this.uiController.gameWon();
	}

	// Used for testing purposes
	public final BoardAsset[][] getCurrentState() {
		return this.currentState;
	}

	// Used for testing purposes
	public BoardAsset[][] getInitialState() {
		return this.INITIAL_STATE;
	}
}