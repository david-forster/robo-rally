package com.robo_rally.game_data;

import java.util.Arrays;
import java.util.HashMap;

import com.robo_rally.base_objects.BoardAsset;
import com.robo_rally.board_objects.Flag;
import com.robo_rally.board_objects.Gear;
import com.robo_rally.board_objects.Pit;
import com.robo_rally.board_objects.Player;
import com.robo_rally.utils.AlertMessage;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Game {

	private Player currentFirstPlayer;
	private HashMap<Player, Player> playerOrder;
	private int maxRounds = 0;
	private Timeline gameTick;
	private String winnerName = "";
	private final Stage primaryStage;

	private Board board;

	public Game(String[] names, char[][][] moves, char[][] board, Stage primaryStage) {
		playerOrder = new HashMap<Player, Player>();
		maxRounds = moves[0].length;
		// Create players and their moves
		// Pass a hashmap of player positions to the board. The board can then pull back
		// the player from the ABCD on board
		this.primaryStage = primaryStage;
		try {
			createBoard(board, createPlayers(names, moves), primaryStage);
		} catch (IllegalArgumentException iae) {
			System.err.println(iae);
		}
	}

	public void start(int roundNumber) {
		gameTick = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
			Player playerToTakeTurn = currentFirstPlayer;
			int methodCount = 0;
			int turnCount = 0;

			@Override
			public void handle(ActionEvent event) {
				if (methodCount % playerOrder.keySet().size() != 0 || methodCount == 0) {
					int[] oldPos = Arrays.copyOf(playerToTakeTurn.getPosition(), 2);
					board.updateLogging(playerToTakeTurn.action());
					board.collisionHandler(playerToTakeTurn, oldPos);
					playerToTakeTurn = playerOrder.get(playerToTakeTurn);

					methodCount++;
				} else if (methodCount % playerOrder.keySet().size() == 0) {
					turnCount++;
					winnerName = board.activate();
					
					if (winnerName != null || (roundNumber == maxRounds && turnCount == 5)) {
						board.gameEnded();
						stopTick(winnerName);
						return;
					}
					currentFirstPlayer = playerOrder.get(currentFirstPlayer);
					playerToTakeTurn = currentFirstPlayer;
					methodCount = 0;
				}
			}
		}));
		
		gameTick.setCycleCount((5 * playerOrder.keySet().size()) + 5);
		gameTick.play();
	}
	
	public void stopTick(String winnerName) { 
		gameTick.stop();
		gameTick = null;
		
		if (winnerName == null) {
			AlertMessage.displayAndMenu("Stalemate! There are no player moves remaining. Try again?", this.primaryStage);
		} else if (winnerName != null) {
			AlertMessage.displayAndMenu("The winner is " + winnerName + "!", this.primaryStage);
		}
	}

	// I have pulled this out of the constructor as a method for readability and
	// easier debugging
	private HashMap<Character, Player> createPlayers(String[] names, char[][][] moves) {
		Player lastPlayer = null;
		HashMap<Character, Player> playerMapping = new HashMap<Character, Player>();
		char position = 'A';
		for (int i = 0; i < names.length; i++) {
			Player player = new Player(names[i], moves[i], position);
			// Check to see if the player has been set
			if (currentFirstPlayer == null || lastPlayer == null) {
				currentFirstPlayer = player;
				lastPlayer = player;
				playerOrder.put(player, null);
				playerMapping.put(position, player);
				position++;
				continue;
			}
			playerOrder.put(lastPlayer, player);
			lastPlayer = player;
			playerMapping.put(position, player);
			position++;
		}
		playerOrder.put(lastPlayer, currentFirstPlayer);

		return playerMapping;
	}

	// The board assets need to be created from a char[][]. Add logic here
	private void createBoard(char[][] board, HashMap<Character, Player> playerMapping, Stage primaryStage) {
		// Create the array based on the current char[][]. If the board is equal, I
		// think that this should work
		BoardAsset[][] initialBoard = new BoardAsset[board.length][board[0].length];
		BoardAsset[][] currentBoard = new BoardAsset[board.length][board[0].length];
		int maxFlag = 0;

		for (int i = 0; i < board.length; i++) {
			for (int k = 0; k < board[i].length; k++) {
				char character = Character.toUpperCase(board[i][k]);
				int[] pos = { i, k };

				// If makes sense here since I can use multiple conditions; rather than case,
				// case, case etc. Still pretty disgusting
				if (character == '.') {
					initialBoard[i][k] = null; // = new EmptyCell
					currentBoard[i][k] = null;
				} else if (character >= 'A' && character <= 'D') {
					if (playerMapping.get(character) != null) {
						Player currentPlayer = playerMapping.get(character);
						currentPlayer.setPosition(pos);
						currentPlayer.setStartPosition(pos);

						initialBoard[i][k] = currentPlayer;
						currentBoard[i][k] = currentPlayer;
					} else {
						throw new IllegalArgumentException("Character [" + character + "] is not one of: A, B, C, D");
					}
				} else if (character == '+' || character == '-') {
					try {
						initialBoard[i][k] = new Gear(character);
						currentBoard[i][k] = new Gear(character);
					} catch (IllegalArgumentException iae) {
						System.err.println(iae);
					}
					continue;
				} else if (Character.getNumericValue(character) >= 1 && Character.getNumericValue(character) <= 4) {
					initialBoard[i][k] = new Flag(Character.getNumericValue(character));
					currentBoard[i][k] = new Flag(Character.getNumericValue(character));
					maxFlag = Character.getNumericValue(character) > maxFlag ? Character.getNumericValue(character)
							: maxFlag;
					continue;
				} else if (character == 'X') {
					initialBoard[i][k] = new Pit();
					currentBoard[i][k] = new Pit();
					continue;
				} else {
					throw new IllegalArgumentException("The board does not follow the specified format!\n" + character
							+ " is not a valid board object");
				}
			}
		}
		this.board = new Board(initialBoard, currentBoard, playerMapping, maxFlag, this, primaryStage);
	}

	// These are required for testing
	public HashMap<Player, Player> getPlayerOrder() {
		return playerOrder;
	}

	public Board getBoard() {
		return board;
	}
}