## Coursework Specification 2019-20 v1.
***Module DC2300 May 10, 2020***

### Contents
1. [**Change History**](#1-change-history)
2. [**Introduction**](#2-introduction)
3. [**Problem Description: Basic Requirements**](#3-problem-description-basic-requirements)
4. [**Problem Description: Advanced Requirements**](#4-problem-description-advanced-requirements)
5. [**Design Notes**](#5-design-notes)
6. [**Deliverables**](#6-deliverables)
7. [**Milestones**](#7-milestones)

#### 1 Change history
V1.0: first version.

#### 2 Introduction
This document contains a specification of the software and other documentation that form the assessed coursework assignment for the module DC2300: Java Program Construction.

The project is to be carried out in small teams: you will see your assignment on Blackboard. I recommend that you set up communication and sharing channels as soon as possible: instant messaging is a good idea, plus some way of sharing files (e.g. Google Drive or Dropbox). If you can, learning a version control system such as Git quickly pays off over using Drive/Dropbox when working on code. GitHub and Gitlab offer free hosting for Git repositories.

Regarding marks, the project will be first marked as a whole, and then your contribution will be factored into the group mark to obtain your individual mark. For that reason, it is important that you provide evidence as to what your contribution was. If you do not contribute, I may remove you from the group: in that case, you will have to complete the coursework by yourself.

If you have any questions on how the coursework runs or what to do, feel free to contact us through the links in the Contacts section on Blackboard.

### 3 Problem Description: Basic Requirements
The task is to implement a grid-based board game, where you race with your robot to touch all the flags on the board in order (first player to do that wins). It is essentially a heavily simplified version of Robo Rally, from Wizards of the Coast: look it up if it sounds interesting!

The robots start on predetermined positions on the board facing north, and a player gets the first player marker. Each round consists of these steps:

1. **The players program their robots with a sequence of 5 different actions. These can be “move forward one space” (F), “Back up one space” (B), “rotate 90 degrees Left” (L), “rotate 90 degrees Right” (R), “do a U-turn” (U), or “Wait” (W).**

    _You may not repeat an action twice in a row: “FFLWF” is not valid, but “FWFLF” would be valid. It would be boring and unfair if everyone simply played “FFFFF” all the time!_

2. **Robots will operate in player order, executing the action in the first slot, then the locations where the robots are will activate, and the first player token pases to the next player. This is repeated with the second, third, fourth and fifth slots.**

    As an example, with player A and player B having decided on “FLFWF” and “WFWFL”, and with A starting with the first player token, this will happen:

    (a) A “F” is executed, then B “W”. Board activates. First player token passes to B.
    
    (b) B “F” is executed, then A “L”. Board activates. First player token passes to A.
    
    (c) A “F” is executed, then B “W”. Board activates. First player token passes to B.
    
    (d) B “F” is executed, then A “W”. Board activates. First player token passes to A.
    
    (e) A “F” is executed, then B “F”. Board activates. First player token passes to B.

There are some additional rules for movement:

- If a robot moves into a position occupied by another robot, it pushes that other robot one space away in the same direction. The robot being pushed may push another robot, and so on.

- If a robot moves outside the board, it is destroyed. Move the robot back to its starting position. If the starting position is occupied, try the position adjacent to the north, then east, south, and finally west.

There are two types of locations that react to robots entering them:

- **Flags** will notify the game that a robot entered it. The game will track if that was the next flag in the sequence for the robot, or not.

- **Pits** will destroy your robot. Move it back to its starting position, just as if it had moved outside the board.

There is one type of location that manipulates robots upon activation, after all robot moves in an action slot have been resolved: **gears**. They can rotate your robot either clockwise or counter-clockwise by 90 degrees.

To activate the board, go through the robots from the top-left corner to the bottom-right, row by row. For each robot, find any special locations where they are and activate them.

A basic version of the game should read the board and the programs from a file. The specific formats for these files are described in Section 5. The basic version of the application would use a text-based user interface (TUI).

The basic requirements allow you to go up to the 60-69 band in the implementation component of the final submission, according to the marking scheme on Table 2. Section 4 describes a set of advanced requirements which will give you more marks in this component, which includes the creation of a graphical user interface.

Remember that whether you use a TUI or a GUI, you should show the evolution of the board over the game. This should be done at least once per round, but you could do it between action slots or even individual actions.

### 4 Problem Description: Advanced Requirements
There are a number of advanced requirements that you could meet to achieve higher marks:

- You can achieve up to 10 extra marks in the implementation component if the game supports both reading programs from files, and reading them directly from the players.

    For a TUI, you will need to add an appropriate way to pick the game mode (whether through a menu or through a command-line switch). For a GUI, you will need to add the UI to enter the actions. In both cases, you will need to tweak the game to run in a stepwise manner, so players may see the consequences of each action.

- You can achieve up to 10 extra marks in the implementation component if the game has a graphical user interface. The GUI should expose all the available functionality in the game. If you do a GUI, you do not need to develop a TUI, but it may be easier/safer to start with a TUI and refactor the code later.

- You can achieve up to 10 extra marks in the implementation component if the **game supports conveyor belts**. Conveyor belts will move your robot one space towards a certain direction, unless there is already a robot there. Conveyor belts in corners will rotate your robot 90 degrees clockwise or counter-clockwise as well, if the robot is carried onto them by a conveyor belt (and **not** if pushed by a robot).

    There is a number of sample boards for this: have a look at them and let us know what you think!

- You can achieve another 10 extra marks in the implementation component if the **game allows robots to be damaged**, regardless of whether you use a TUI or a GUI. Robots will start with 5 health, and they will be equipped with lasers.

    The game will be extended with a laser firing phase, after the gears and conveyor belts (if you have them) have been activated:
    1. First, robots will fire their lasers in player order: the first robot standing on the line of sight of the firing robot will receive one point of damage.

    1. Second, if a robot is standing in the middle of a board laser beam, they will also take 1 point of damage. If two or more robots are standing in the way of a board laser beam, only the robot closest to the emitter is affected: it is stopping the beam before it reaches the others.

Robots with _n_ health can only do their _n_ first actions. If a robot’s health reaches zero, it is destroyed and it returns to its starting point (or a free adjacent position in NESW order, if not available) with full health and facing north.

For this one, you will have to come up with your own sample boards. Make sure to write about how you designed those sample boards in your design report!

### 5 Design Notes
The following information and ideas may be useful in developing the coursework. You will also find some of the ideas and structures from the lab classes helpful.

- The program should allow for setting up the board, the number of players, the names of the players and the programs given by the players to their robots. You could do this through a textual interface, through a graphical interface, and/or by loading text files. In fact, we have prepared a collection of sample **.brd** board files and **.prg** program files. 

_Examples are shown on Listings 1 and 2 (have to refer to original PDF for listings)._
    
You are heavily recommended to reuse these formats for your coursework. This would allow you to share boards between groups and check against each other if you have implemented the game behaviour correctly.

For board files, we are going to follow a simple text-based format. The file will start with a line declaring the format version (a common practice for future-proofing the format), which we can ignore. After that, we will have a sequence of lines of characters: each character is a cell in the grid, and all lines must have the same number of characters. The available characters are as follows:
- A period (.) represents a position with no locations (it is empty).
- **A** to **D** are the starting positions of the maximum 4 players we can have in the game.
- **+** and **-** are gears (+ rotates clockwise,- rotates counter-clockwise).
- **1** to **4** are the maximum 4 flags you can have, to be visited in this order.
- **x** is a pit, which destroys your robot and returns it to its starting position.
- (Optional: if doing conveyor belts) **v**, **>** , **<** and are straight conveyor belts which upon activation, move your robot one space down, right, left or up, respectively.
- (Optional: if doing conveyor belts) **N**, **E**, **S** and **W** are the clockwise corner conveyor belts, and **n**, **e**, **s** and **w** are the counter-clockwise corner conveyor belts. The clockwise conveyor belts will turn your robot to the right if and only if it is carried onto that location by another conveyor belt. Likewise, the counter-clockwise conveyor belts will turn the robot to the left on that situation.

    Upon activation,**N**/**n** will move the robot north, **S**/**s** south, **W**/**w** west, and **E**/**e** east.

    In the example above, the conveyor on the left would have the robot go on a counter-clockwise loop, and the conveyor on the right would do a clockwise loop. You would need a carefully timed entry and jump off the loop on the left to reach the second flag!

- (Optional: if doing lasers) Lasers will span from an emitter **[** to a receiver **]** horizontally (left to right), or from an emitter **(** to a receiver **)** vertically (top to bottom).

You can reuse the text-based format from above if you want to show the board in a text-based user interface. If there is a robot and something else at the same position on the board (e.g. a gear), show only the robot as “A”, “B”, “C” or “D”, depending on the player.

The format for the program file is much simpler. You have the same format version declaration line, a line with the player names separated by spaces, and then a sequence of lines with the programs from the different players in each round. Each line is formed of groups of 5 characters, separated by single spaces. The first group will be the program of player A, then player B, and so on. This means that the order of the groups is based on the identities of the players, not their turn order in
that round!

- You must develop all the classes in your team, with the exception of the Java standard library classes (e.g. collections, JavaFX, math routines).

- You will want to talk amongst yourselves to find your relative strengths and weaknesses, and play on those. Some of your team members may be better at doing the analysis, while others may do better at coding, designing the UI, coming up with tests to run or writing good reports.

- You will want to create a _Gridclass_, and have a base _GridEntity_ type (which may be a class or just an interface) that represents anything that can go on the grid and can be asked to act() at a certain point. The various location types and the robots themselves should be based on this
common type.

- You may want to have the actions that can be picked by the players as their own objects, in their own hierarchy. This would make it easier to support new player actions in the future without having to modify the rest of the codebase.

- You may want to support a single-player mode to make it easier to test the basic board logic. Do not forget to test the main multiplayer mode, though!

- You should keep the core of the game separate from the actual UI. For a TUI, have different classes read the grid and render it into text. For a GUI, have the graphical interface redraw the grid from action to action, or make the gridobservableso the GUI reacts naturally to any changes. The second option is more elegant to use but more challenging to implement.

- Beyond the provided sample boards and programs, you should try creating your own small boards and programs that demonstrate that the game works as it should. Same goes for robots pushing each other in various directions. Do not forget to test when the robot falls off the board — it should be destroyed and go back to its starting position (or a free adjacent position in NESW order).

- You should think of a way to cleanly divide the work across the team, in a way that allows for easily integrating what you do on your own, and which allows for several people to work on things at the same time. For instance, you could break the work into more or less these “chunks” or subsystems:
    - File input and output for each type of file.

    - The different types of locations you can have on the grid.

    - The _Robot_ entity and the management of the global game state (e.g. flags touched).
    
    - Any extra requirements (lasers, conveyor belts).
    
    - (Graphical and/or textual) user interface for configuration.
    
    - (Graphical and/or textual) user interface for visualisation of the running game.

- Don’t jump into coding straight away if you are not experienced with object-oriented programming. Use responsibility-driven design to draw out a good distribution of responsibilities and come up with a good set of candidates, create a skeleton with only empty methods, and once you have agreed on the skeleton have different team members fill in the blanks.

- Read each other’s code, and test the classes of other team members! A bit of pair programming will be great, especially if you have people with very different levels of experience in coding in the team. The more experienced person can learn a lot when teaching another: it’s not a one-way street, and in your future job you will eventually be mentoring your juniors anyway.

    The idea is that one person types the code (the driver) and focuses on the low-level details, while another person thinks about the overall task and how the code fits into the intended design (the co-pilot). It’s best if the more experienced person acts as the co-pilot, though you may want to rotate roles every 20-30 minutes.

- The submission should provide evidence that all team members contributed roughly equally although they may have had different roles. If team members do not contribute equally, then there should be evidence that they were invited to do so.
    In terms of evidence, you can provide a list of project meeting minutes with dates, attendees, and evidence that the meetings were announced to everyone in the group. If one of your members is not confident about programming, try including them in a session ofpair programmingwith someone who is more confident.

### 6 Deliverables
The marks for the coursework will be divided as follows:

- CRC cards: good use of responsibility-driven design (10%)
- UML diagrams: good use of notation and strong link to CRC cards (15%)
- Design of your code: is it good, object-oriented code? (15%)
- Implementation: working game (45%)
- Testing: unit tests in JUnit with good coverage (15%)

The marking schemes for the various components are listed in Tables 1 and 2. By **Friday August 21st 23:59 BST**, your group must upload to Blackboard a ZIP with:

- A PDF or a set of photographs with your Candidate/Responsibility/Collaboration (CRC) cards detailing what each class should do (at the analysis level). You do not need to worry about the TUI/GUI classes, only the domain of the problem itself. For instance, aGameclass aggregating the state of the game makes sense at this analysis stage, but not a _MainWindow_ class nor a _TextMenu_ class.
- A set of pictures with UML class diagrams of the intended domain classes for the system. We heavily suggest you use a “proper” UML tool (e.g. PlantText, Eclipse Papyrus, GenMyModel) or at least a UML-aware drawing tool (Umlet, draw.io). Generally, extracting it from your code does not produce good results.

    Usually, it is better to use one UML diagram per subsystem rather than trying to put everything in the same diagram. The same class can appear in multiple diagrams if it makes sense: usually it will be fully detailed in one and simplified in the others.

***Mark scheme is available on PDF Brief***

- A short (2 pages max) description of the overall design of your system. What are the various parts, and how are they related to each other? Have you followed any design patterns? How have you kept cohesion high and coupling low?
- An executable Eclipse project with the final version of the simulation, and a “good” test suite written with JUnit. With “good”, we mean that it covers the most important parts of the functionality of your system:
    - Does the system reject invalid configurations?
    - Are out of bounds robots detected and handled correctly?
    - Are robots moving as expected?
    - Are gears rotating robots as expected?
    - Is the winning condition detected correctly?
    - Are conveyor belts rotating and carrying robots as expected?
    - Are lasers firing correctly, and are beams being blocked as expected?
    - ... and so on. Think of other things that ought to work.

    You do not need to test trivial parts (e.g. getters/setters). You should have tests for the “happy” scenarios, and for failure scenarios as well. Using a test coverage tool to find tests you may be missing is recommended.

    We _heavily_ recommend you do these at the same time you develop the rest of the system. It is usually better to gradually build and test your system up, rather than building everything and then praying that it works!

Additionally, each team member will need to submit by **Friday 21st August 23:59 BST** a PDF with their individual reflection on how the project went, and a statement on their contribution to the overall work.

Late submissions will be treated under the standard rules for Computer Science, with an **absolute** deadline of one week after which submissions will not be marked. This is necessary so that feedback can be given before the start of exams and to spread out coursework deadlines. The lateness penalty will be 10% of the available marks for each working day.

### 7 Milestones
Experience has shown that the most successful groups are those that work together in a structured way and follow a sensible lifecycle. To encourage this, I propose the following milestones.

**Requirements Analysis:** you need to analyse the requirements defined in this document to generate an overall system architecture. You should start this right after the webinar on RDD (May 14th). We will ask you to submit your CRC card drafts on May 21st - these will not be for marks, but we will review a sample of them and discuss them in the next webinar (May 28th).

**Design:** you need to create a UML class diagram for your detailed design. You should start this as soon as the analysis is ready: you should send your initial drafts by June 4th, so that we may give you some feedback in the next webinar (June 11th).

**Implementation:** you can start on this as soon as you are happy with your UML diagrams and your overall distribution of responsibilities. I recommend that you start with a simple single-player mode where you just move a robot on an empty board, and then gradually add the rest of the remaining functionality (falling off the board, then the various location types, then a second player, then turn rotation, and so on). You should have a first version that you can start writing tests for once we teach TDD by the end of July.

**GUI Design:** some of your team members could start on it as soon as we complete the JavaFX unit that takes place on the week commencing June 15th.

You should discuss personal holiday plans over the summer, and agree on intermediate delivery mile-stones with your teammates to avoid last minute “big bang” integrations. As you will know from your job, these never go well!
